-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2018 at 05:02 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `productlist`
--

CREATE TABLE `productlist` (
  `id` int(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `sku` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `size` int(10) DEFAULT NULL,
  `weight` int(10) DEFAULT NULL,
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `length` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productlist`
--

INSERT INTO `productlist` (`id`, `type`, `sku`, `name`, `price`, `size`, `weight`, `height`, `width`, `length`) VALUES
(78, 'DVD-disc', 'JVC200123', 'acme DISC', '1.00', 700, 0, 0, 0, 0),
(104, 'DVD-disc', 'JVC200123', 'Acme DISC', '1.00', 700, 0, 0, 0, 0),
(105, 'DVD-disc', 'JVC200123', 'Acme DISC', '1.00', 700, 0, 0, 0, 0),
(106, 'DVD-disc', 'JVC200123', 'Acme DISC', '1.00', 700, 0, 0, 0, 0),
(107, 'DVD-disc', 'JVC200123', 'Acme DISC', '1.00', 700, 0, 0, 0, 0),
(109, 'book', 'GGWP0007', 'War and Peace', '20.00', 0, 2, 0, 0, 0),
(110, 'book', 'GGWP0007', 'War and Peace', '20.00', 0, 2, 0, 0, 0),
(111, 'book', 'GGWP0007', 'War and Peace', '20.00', 0, 2, 0, 0, 0),
(112, 'book', 'GGWP0007', 'War and Peace', '20.00', 0, 2, 0, 0, 0),
(115, 'furniture', 'TR120555', 'Chair', '40.00', NULL, NULL, 24, 45, 15),
(116, 'furniture', 'TR120555', 'Chair', '40.00', NULL, NULL, 24, 45, 15),
(117, 'furniture', 'TR120555', 'Chair', '40.00', NULL, NULL, 24, 45, 15),
(118, 'furniture', 'TR120555', 'Chair', '40.00', NULL, NULL, 24, 45, 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `productlist`
--
ALTER TABLE `productlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `productlist`
--
ALTER TABLE `productlist`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
