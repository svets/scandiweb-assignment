function validation() {
    var type = document.getElementById('TypeSwitcher').value;
    var sku = document.getElementById('sku').value;
    var name = document.getElementById('name').value;
    var price = document.getElementById('price').value;
    var size = document.getElementById('size').value;
    var weight = document.getElementById('weight').value;
    var height = document.getElementById('height').value;
    var length = document.getElementById('length').value;
    var metrics = size + weight + height + length;

    if(type=="" || type==null || sku=="" || sku==null || name=="" || sku==null || price=="" || price==null || metrics=="" || metrics==null || metrics<1) {
        alert('Please fill in all necessary data');
        return false;
    }
    else {
        return true;
    }
}


function submitted(){
    setTimeout(function() {
        location.href="list.php"
    }, 2000 );
}