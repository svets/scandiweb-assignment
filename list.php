<?php
include('db_connect.php');
include('product_delete.php');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Product New</title>
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/custom.css">
		<!-- JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="js/jqueryfunctions.js"></script>
		<script src="js/functions.js"></script>
	</head>
	<body>
		<header id="navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-6 prod-list">
						<h1>Product List</h1>
					</div>
					<div class="col-md-6 text-right prod-list">
						<!-- Page will reload after 2 seconds to show which db entries were left after delete -->
						<form name="form1" action="" method="post" onsubmit="submitted();">
							<button type="button" class="btn dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" >
								Mass Delete Action
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<!-- Submit button to delete db entries -->
							<input type="submit" name="submit1" value="Apply" class="btn btn-danger"></input>
					</div>
				</div>
			</div>
		</header>
		<section>
			<div class="container">
				<hr class="style">
			</div>
		</section>
		<section id="main-area">
			<div class="container">
				<div class="row">
				<?php while ($products = mysqli_fetch_assoc($result)) { ?>
					<div class="col-lg-3 col-md-4 col-sm-6">
						<div class="grey-box">
							<div>
								<input type="checkbox" name="num[]" id="type" value="<?php echo $products['id']; ?>">
							</div>
							<div class="text-center">
								<p><?php echo $products['sku']; ?></p>
								<p><?php echo $products['name']; ?></p>
								<p><?php echo $products['price'] . "$"; ?></p>
								<p><?php
										if ($products['size'] > 0) {
											echo "Size: " . $products['size'] . " MB"; }
									?>
								</p>
								<p><?php if ($products['weight'] > 0) {
											echo "Weight: " . $products['weight'] . " KG"; }
									?>
								</p>
								<p><?php if ($products['width'] > 0 && $products['height'] > 0 && $products['length'] > 0) {
										echo " W:" . $products['width'];
										echo " H:" . $products['height'];
										echo " L:" . $products['length']; }
									?>
								</p>
							</div>
						</div>
					</div>
				<?php } ?>
				</form>
				</div>
			</div>
		</section>
	</body>
</html>