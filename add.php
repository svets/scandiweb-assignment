<?php
include('db_connect.php');
include('product_add.php');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Product New</title>
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/custom.css">
		<!-- JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="js/jqueryfunctions.js"></script>
		<script src="js/functions.js"></script>
	</head>
	<body>
		<form action="" method="post" onsubmit="return validation();">
		<header id="navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-6 prod-list">
						<h1>Product Add</h1>
					</div>
					<div class="col-md-6 text-right prod-list">
						<!-- Submit button -->
						<input type="submit" id="insert" name="insert" class="btn btn-danger" value="Save">
					</div>
				</div>
			</div>
		</header>
		<section>
			<div class="container">
				<hr class="style">
			</div>
		</section>
		<section>
			<div class="container">
				<div class="form-group col-md-4">
					<label>SKU</label>
					<input type="text" id="sku" name="sku" required class="form-control">
					<label>Name</label>
					<input type="text" id="name" name="name" required class="form-control">
					<label>Price</label>
					<input type="text" id="price" name="price" required class="form-control">
					<!-- Type Switcher -->
					<label for="TypeSwitcher">Type Switcher</label>
					<select class="form-control" id="TypeSwitcher" name="type">
						<option hidden>Type Switcher</option>
						<option value="disc">DVD-disc</option>
						<option value="book">Book</option>
						<option value="furniture">Furniture</option>
					</select>
					<div id="disc" class="attributes disc">
						<label>Size</label>
						<input type="number" id="size" name="size" class="form-control" />
						<br>
						<p>Please provide size in MB format</p>
					</div>
					<div id="book" class="attributes book">
						<label>Weight</label>
						<input type="number" id="weight" name="weight" class="form-control" />
						<br>
						<p>Please provide height in KG format</p>
					</div>
					<div id="furniture" class="attributes furniture">
						<label>Height</label>
						<input type="number" id="height" name="height" class="form-control" />
						<br>
						<label>Width</label>
						<input type="number" id="width" name="width" class="form-control" />
						<br>
						<label>Length</label>
						<input type="number" id="length" name="length" class="form-control" />
						<br>
						<p>Please provide dimensions in HxWxL format</p>
					</div>
					</div>
				</div>
			</section>
		</form>
	</body>
</html>